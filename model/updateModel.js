var mongoose = require('mongoose');
var updateSchema = new mongoose.Schema({
    view: {type: Number, "default": -1},
    backoff: Number,
    createOn: {type: Date, "default": Date.now}
})
mongoose.model('Update', updateSchema);