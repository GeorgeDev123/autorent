﻿var global = require('./global.js')
var browser = global.driver.browser;
var webdriver = global.driver.webdriver;
var backoffasync = function () {
  var btnRefresh = browser.wait(webdriver.until.elementLocated(webdriver.By.id('k_refresh')), 50000);
  btnRefresh.click();
  var alertMssg = browser.wait(webdriver.until.elementLocated(webdriver.By.className('alert_right')), 50000);
  var txtValue = alertMssg.getText();
  var content = txtValue;
  var backOff = content.match(/\d/g).join("");
  var closebtn = browser.findElement(webdriver.By.id('closebtn'));
  closebtn.click();
  return backOff;
};
var updateRent = (function () {
  var backoff = function () {
    browser.wait(webdriver.until.elementLocated(webdriver.By.id('k_refresh')), 50000).then(function (btnRefresh) {
      btnRefresh.click().then(function () {
        browser.wait(webdriver.until.elementLocated(webdriver.By.className('alert_right')), 50000).then(function (alertMssg) {
          alertMssg.getText().then(function (txtValue) {
            var content = txtValue;
            var backOff = content.match(/\d/g).join("");
            browser.findElement(webdriver.By.id('closebtn')).click();
            return backOff;
          });
        }, function (err) {
          onError(err);
          return 60;
        });
      });
    }, function (err) {
      console.log('something went wrong');
      return 60;
    });
  };
  var add = function (a,b){
    return a + b;
  }
  return {
    getBackOff: backoff,
    add: add
  };
})();

module.exports.update = function (){
  return backoffasync();
}
module.exports.add = function (a, b) {
  return updateRent.add(a,b);
}
var onError = function(err){
    console.log('error occured details below: ');
    console.log(err);
}