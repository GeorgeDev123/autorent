﻿var global = require('./global.js')
var browser = global.driver.browser;
var webdriver = global.driver.webdriver;
var driver = global.driver.driver;
var wd = global.driver.wd;
var async = require('async');
var updateServices = require('./services/updateservices.js');
var eleUpdate = {
  btnRefresh: 'k_refresh',
  alertMssg: 'alert_right'
}
function waitById(id){
  wd.wait(function () {
    return driver.findElements(wd.By.id(id)).length > 0;
  }, { timeout: 50000, period: 1000 });
}
function waitByClassName(className){
  wd.wait(function () {
    return driver.findElements(wd.By.className(className)).length > 0;
  }, { timeout: 50000, period: 1000 });
}
function getViews(){
  var viewParent = driver.findElement(wd.By.cssSelector('.hm.ptn'));
  var views = viewParent.findElements(wd.By.className('xi1'));
  var view = views[0];
  var viewNumber = parseInt(view.getText());
  console.log(viewNumber + ' people had viewed it');
  return viewNumber;
}
function periodUpdate(url){
  var backoff = 0;
  async.forever(
    function (next) {
      setTimeout(function () {
        waitById(eleUpdate.btnRefresh);
        try {
          driver.navigate().refresh();
        }
        catch (e){
          var errorDate = new Date();
          console.log('refresh error' + e + errorDate);
          backoff = 5;
          next();
          return;
        }
        waitById(eleUpdate.btnRefresh);
        var views = getViews();
        driver.findElement(wd.By.cssSelector('a[href*="action=refresh"]')).click();
        try {
          waitByClassName(eleUpdate.alertMssg);
        }
        catch (e){
          var errDate = new Date();
          console.log('this eerror again ' + e + errDate);
          var erroMssg = driver.findElement(wd.By.className('alert_error')).getText();
          if(erroMssg !== undefined) {
            backoff = parseInt(erroMssg.match(/\d/g).join("")) + 10;
            console.log(erroMssg + errDate)
          }
          else {
            backoff = 35;
            console.log('worst case so wait 35 minutes' + errDate);
          }
          next();
          return;
        }
        var content = driver.findElement(wd.By.className(eleUpdate.alertMssg)).getText();
        console.log(content);
        backoff = parseInt(content.match(/\d/g).join("")) + 10;
        driver.findElement(wd.By.id('closebtn')).click();
        var createOn = new Date();
        //updateServices.rentUpdate(views, backoff, createOn);
        next();
      }, backoff * 60 * 1000);
    },
    function (err) {
      console.log(err);
    }
  );
}
module.exports.Init = function (url, credential, loginElements) {
  driver.get(url);
  driver.findElement(wd.By.id(loginElements.eleUserName)).sendKeys(credential.userName);
  driver.findElement(wd.By.id(loginElements.elePassword)).sendKeys(credential.password);
  driver.findElement(wd.By.cssSelector(loginElements.eleLogin)).click();
  periodUpdate(url);
};